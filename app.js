const qrcode = require('qrcode-terminal');

const { Client, MessageMedia } = require('whatsapp-web.js');
const client = new Client();

client.on('qr', (qr) => {
    qrcode.generate(qr, { small: true });
});

client.on('ready', () => {
    console.log('Client is ready!');
});

client.on('message', async msg => {
    if (msg.body == '!ping') {
        msg.reply('pong');
    }
    if (msg.body == '#sticker') {
        if (msg.hasMedia) {
            const attachmentData = await msg.downloadMedia();
            msg.reply(attachmentData, null, {sendMediaAsSticker: true});
        } else if (msg.isGif) {
            await msg.downloadMedia().then((attachmentData) => {
                msg.reply(attachmentData, null, {sendMediaAsSticker: true})
            }, (rejected) => {
                console.info(rejected);
            });
        }
    }
});

client.initialize();
